<!-- meta page description: wxGUI Graphical Modeler -->
<!-- meta page index: topic_gui|GUI -->
<h2>DESCRIPTION</h2>

<p>
The <b>Graphical Modeler</b> is
a <em><a href="wxGUI.html">wxGUI</a></em> component which allows the user to
create, edit, and manage complex models using easy-to-use
interface. When performing analytical operations in GRASS, the
operations are not isolated, but part of a chain of operations. Using
Graphical Modeler, that chain of processes (ie. GRASS modules)
can be wrapped into one process (ie. model). So it's easier to execute
the model later with slightly different inputs or parameters.
<br>
Models represent a programming technique used in GRASS GIS to
concatenate models together to accomplish a task. It is advantageous
when user see boxes and ovals that are connected by lines and
represent some tasks rather than seeing lines of coded text. Graphical
Modeler can be used as custom tool that automates a process. Created
model can simplify or shorten a task can be run many times and it can
also be shared with others. Important note is that models cannot
perform specified tasks that one cannot perform manually with GRASS
GIS. It is recommended to first do process manually, note the steps
(eg. using Copy button in module dialogs) and later duplicate them in
model.

<p>
The Graphical Modeler allows you to:

<ul>
  <li>define data items (raster, vector, 3D raster maps)</li>
  <li>define actions (GRASS commands)</li>
  <li>define relations between data and action items</li>
  <li>define loops (eg. map series) and conditions (if-else statements)</li>
  <li>define model variables</li>
  <li>parameterize GRASS commands</li>
  <li>define intermediate data</li>
  <li>validate and run model</li>
  <li>store model properties to the file (<i>GRASS Model File|*.gxm</i>)</li>
  <li>export model to Python script</li>
  <li>export model to image file</li>
</ul>

<h3>Main dialog</h3>

Graphical Modeler can be launched from the Layer Manager menu
<tt>File -&gt; Graphical modeler</tt> or from the main
toolbar <img src="icons/modeler-main.png" alt="icon">. It's also
available as stand-alone module <em>g.gui.gmodeler</em>.

<p>
The main Graphical Modeler menu contains options which enable the user
to fully control the model. Directly under the main menu one can find
toolbar with buttons (see figure below). There are options like
(<font size="2" color="red">1</font>) Create new model,
(<font size="2" color="red">2</font>) Load model from file,
(<font size="2" color="red">3</font>) Save current model to file,
(<font size="2" color="red">4</font>) Export model to image,
(<font size="2" color="red">5</font>) Export model to Python script,
(<font size="2" color="red">6</font>) Add command (GRASS modul) to
model, (<font size="2" color="red">7</font>) Add data to model,
(<font size="2" color="red">8</font>) Manually define relation between
data and commands, (<font size="2" color="red">9</font>) Add
loop/series to model, (<font size="2" color="red">10</font>) Add
comment to model, (<font size="2" color="red">11</font>) Redraw model
canvas, (<font size="2" color="red">12</font>) Validate model,
(<font size="2" color="red">13</font>) Run model, (<font size="2"
color="red">14</font>) Manage model variables, (<font size="2"
color="red">15</font>) Model settings, (<font size="2"
color="red">16</font>) Show manual and last of them is button
(<font size="2" color="red">17</font>) Quit Graphical Modeler.

<p>
<center>
<img src="g_gui_gmodeler_toolbar.png">
<br>
<i>Figure: Components of Graphical Modeler menu toolbar.</i>
</center>

<p>
There is also lower menu bar in Graphical modeler dialog where one can
manage model items, see commands, add or manage model variables,
define default values and descriptions. Python editor dialog window
allows seeing performation written in Python code. The rightmost tab of
the bottom menu is automatically triggered when the model is activated and
shows all the steps of running GRASS modeler modules. In case of errors
in the calculation process, it is written at that place.

<center>
<a href="g_gui_gmodeler_lower_toolbar.png"><img src="g_gui_gmodeler_lower_toolbar.png" width="700"></a>
<br>
<i>Figure: Lower Graphical Modeler menu toolbar.
</i>
</center>

<h3>Components of models</h3>

The workflow is usually established from four types of diagrams. Input and derived 
model data are usually represented with oval diagram. 
This type of model elements stores path to 
specific data on the user's disk. It is possible to insert vector data, raster data, 
database tables, etc. The type of data is clear distinguishable in the model by
its color. Different model elements are shown in the figures below.

<ul>
  <li> (<font size="2" color="red">A</font>) raster data: <img src="g_gui_gmodeler_raster.png" alt="raster" style="margin: 0px 0px -5px 0px">
  <li> (<font size="2" color="red">B</font>) relation: <img src="g_gui_gmodeler_relation.png" alt="relation" style="margin: 10px 0px 0px 0px">
  <li> (<font size="2" color="red">C</font>) GRASS module: <img src="g_gui_gmodeler_modul.png" alt="module" style="margin: 0px 0px -5px 0px">
  <li> (<font size="2" color="red">D</font>) loop: <img src="g_gui_gmodeler_loop.png" alt="loop" style="margin: 15px 0px -5px 0px">
  <li> (<font size="2" color="red">E</font>) database table: <img src="g_gui_gmodeler_db.png" alt="db" style="margin: 10px 0px -5px 0px">
  <li> (<font size="2" color="red">F</font>) 3D raster data: <img src="g_gui_gmodeler_raster3d.png" alt="raster3D" style="margin: 10px 0px -5px 0px"> 
  <li> (<font size="2" color="red">G</font>) vector data: <img src="g_gui_gmodeler_vector.png" alt="vector" style="margin: 10px 0px -5px 0px">  
  <li> (<font size="2" color="red">H</font>) disabled GRASS module: <img src="g_gui_gmodeler_modulex.png" alt="module" style="margin: 10px 0px -5px 0px"> 
  <li> (<font size="2" color="red">I</font>) comment: <img src="g_gui_gmodeler_comment.png" alt="comment" style="margin: 10px 0px -5px 0px">  
</ul> 

<center>
<a href="g_gui_gmodeler_model_classification.png">
<img src="g_gui_gmodeler_model_classification.png" width="500"></a>
<br>
<i>Figure: A model to perform unsupervised classification using MLC
(<a href="i.maxlik.html">i.maxlik</a>) and SMAP (<a href="i.smap.html">i.smap</a>).
</i>
</center>

<p>
Another example:

<center>
<a href="g_gui_gmodeler_model_usle.png">
<img src="g_gui_gmodeler_model_usle.png" width="600"></a>
<br>
<i>Figure: A model to perform estimation of average annual soil loss
caused by sheet and rill erosion using The Universal Soil Loss
Equation.</i>
</center>

<p>
Example as part of landslide prediction process:

<center>
<a href="g_gui_gmodeler_model_landslides.png">
<img src="g_gui_gmodeler_model_landslides.png" width="600"></a>
<br>
<i>Figure: A model to perform creation of parametric maps used by geologists 
to predict landlides in area of interest.</i>
</center>

<h2>EXAMPLE</h2>

In this example the <tt>zipcodes_wake</tt> vector data and the
<tt>elev_state_500m</tt> raster data from the North Carolina
sample dataset (original <a href="http://grass.osgeo.org/sampledata/north_carolina/nc_rast_geotiff.zip">raster</a> and
<a href="http://grass.osgeo.org/sampledata/north_carolina/nc_shape.zip">vector</a>
data) are used to calculate average elevation for every
zone. The important part of the process is Graphical Modeler, namely its
possibilities of process automation.

<p>
In command console the procedure would be as follows:

<div class="code"><pre>
# input data import
r.import input=elev_state_500m.tif output=elevation 
v.import input=zipcodes_wake.shp output=zipcodes_wake 
# computation region settings
g.region vector=zipcodes_wake  
# raster statistics (average values), upload to vector map table calculation
v.rast.stats -c map=zipcodes_wake raster=elevation column_prefix=rst method=average 
# univariate statistics on selected table column for zipcode map calculation
v.db.univar map=zipcodes_wake column=rst_average 
# conversation from vector to raster layer (due to result presentation)
v.to.rast input=zipcodes_wake output=zipcodes_avg use=attr attribute_column=rst_average 
# display settings
r.colors -e map=zipcodes_avg color=bgyr                                         
d.mon start=wx0 bgcolor=white                                                   
d.barscale style=arrow_ends color=black bgcolor=white fontsize=10
d.rast map=zipcodes_avg bgcolor=white                                                                                                 
d.vect map=zipcodes_wake type=boundary color=black                                                     
d.northarrow style=1a at=85.0,15.0 color=black fill_color=black width=0 fontsize=10
d.legend raster=zipcodes_avg lines=50 thin=5 labelnum=5 color=black fontsize=10 
</pre></div>

To start performing above steps as an automatic process with Graphical Modeler
press the <img src="icons/modeler-main.png" alt="icon"> icon or
type <em>g.gui.gmodeler</em>. The simplest way of inserting elements
is by adding complete GRASS command to Command field in GRASS command
dialog (see figure below).  With full text search one can faster
module hunting. Then label and command can be added. In case that only
module name is inserted, after <i>Enter</i> button pressing, module
dialog window is displayed and it is possible to set all of usual
module options (parameters and flags).

<p> 
<center>
<a href="g_gui_gmodeler_dlg_module.png"><img src="g_gui_gmodeler_dlg_module.png"
width="400"></a>
<br>
<i>Figure: Dialog for adding GRASS commands to model.</i>
</center>

<p>  
All used modules can be parameterized in the model. That causes launching the
dialog with input options for model after the model is run. In this example
input layers (<tt>zipcodes_wake</tt> vector data and <tt>elev_state_500m</tt> 
raster data) are parameterized. Parameterized elements have a little thicker
boarder in the model scheme with diagrams.

<center>
<a href="g_gui_gmodeler_parameter.png">
<img src="g_gui_gmodeler_parameter.png" width="500"></a>
<br>
<i>Figure: Model parameter settings.</i>
</center>

<p>
The final model, the list of all model items, and the Python code window with
<i>Save</i> and <i>Run</i> option are in the figures below.

<center>
<a href="g_gui_gmodeler_model_avg.png">
<img src="g_gui_gmodeler_model_avg.png" width="600"></a>
<br>
<i>Figure: A model to perform average statistics for zipcode zones.</i>
</center>
<br>
<center>
<a href="g_gui_gmodeler_items.png">
<img src="g_gui_gmodeler_items.png" width="600"></a>
<p>
<a href="g_gui_gmodeler_python.png">
<img src="g_gui_gmodeler_python.png" width="600"></a>
<br>
<i>Figure: Items with Python editor window.</i>
</center>

<p>
The resultant model for Graphical Modeler is available
<a href="g_gui_gmodeler_zipcodes_avg_elevation.gxm">here</a>.

<p>
After model is run with <img src="icons/execute.png" alt="run"> button
and inputs are set, results can be displayed as follows:

<center>
<a href="g_gui_gmodeler_avg_run.png"><img src="g_gui_gmodeler_avg_run.png" width="500"></a>
<a href="g_gui_gmodeler_avg_map.png"><img src="g_gui_gmodeler_avg_map.png" width="300"></a>
<br>
<i>Figure: Average elevation for zipcodes using North Carolina sample dataset as 
automatic calculation performed by Graphical Modeler.</i>
</center>

<p>
When one wants to run model again with the same data or the same names, it is 
necessary to use <tt>--overwrite</tt> option. It will cause maps with identical 
names to be overwritten. Instead of setting it for every 
module separately it is handy to change Model Property settings globally.
This dialog includes also metadata settings, where model name, model description 
and autor(s) of model can be set.

<center>
<a href="g_gui_gmodeler_model_properties.png">
<img src="g_gui_gmodeler_model_properties.png" width="350"></a>
<br>
<i>Figure: Model properties.</i>
</center>

<p>
Another useful trick is the possibility to set variables. Their content can be used 
as a substitute for other items. Value of variables can be values such as 
raster or vector data, integer, float, string value or they may constitute some 
region, mapset, file or direction data type.   

Then it is not
necessary to set any parameters for input data. Dialog with viariable settings 
is automatically displayed after model is run. So, instead of Model parameters 
(e.g. <tt>r.import</tt> a <tt>v.import</tt>, see 
<em><a href="g_gui_gmodeler_avg_run.png">Run model dialog</a></em> above) 
there are <tt>Variables</tt>.

<center>
<a href="g_gui_gmodeler_variables_run.png">
<img src="g_gui_gmodeler_variables_run.png" width="500"></a>
<br>
<i>Figure: Model with variable inputs.</i>
</center>

<p>
The key point is usage of <tt>%</tt> before the substituting variable and 
settings in Variables dialog. For example, when there is a model variable 
<tt>raster</tt> that references a file path and that value is required to be 
used as one of inputs to a particular model, it should be specified in Variable 
dialog with competent name (<tt>raster</tt>), data type, default value and 
description. Then it should be set in module dialog as input called 
<tt>%raster</tt>.

<center>
<a href="g_gui_gmodeler_variables.png">
<img src="g_gui_gmodeler_variables.png" width="600"></a>
<br>
<i>Figure: Example of raster file variable settings.</i>
</center>

<br>
<center>
<a href="g_gui_gmodeler_variables_raster.png">
<img src="g_gui_gmodeler_variables_raster.png" width="600"></a>
<br>
<i>Figure: Example of raster file variable usage.</i>
</center>

<p>
Finally, the model settings can be stored to the file as a GRASS GIS Model 
File <tt>*.gxm</tt> what represents very handy advantage. It can be shared as reusable 
workflow that be run by different users with different data.

<p>
For example, this model can later be used to calculate (let's say)
average precipe value for every administrative region in Slovakia
using <tt>precip</tt> raster data from
<a href="https://grass.osgeo.org/uploads/grass/sampledata/slovakia3d_grass7.tar.gz">
Slovakia precipitation dataset</a> and administration boudaries of Slovakia from 
<a href="https://www.geoportal.sk/sk/zbgis_smd/na-stiahnutie/">Slovak Geoportal</a>
(only with a few clicks).

<p>
There can be some data in a model that did not exist before the process and 
that it is not worth it to maintain after the process executes. They can 
be described as being <tt>Intermediate</tt> by single clicking using the right 
mouse button, see figure below. All such data should be deleted following 
model completion. The boundary of intermediate component is dotted line.

<center>
<a href="g_gui_gmodeler_intermediate_data.png">
<img src="g_gui_gmodeler_intermediate_data.png" width="400"></a>
<br>
<i>Figure: Usage and definition of intermediate data in model.</i>
</center>
 
<p>
Using Python editor in wxGUI Graphical Modeler one can add python code and then 
run it with <tt>Run</tt> button or just save it as python script <tt>*.py</tt>.
Result is shown below.

<center>
<a href="g_gui_gmodeler_python_code.png">
<img src="g_gui_gmodeler_python_code.png" width="350"></a>
<a href="g_gui_gmodeler_python_code_result.png">
<img src="g_gui_gmodeler_python_code_result.png" width="350"></a>
<br>
<i>Figure: Python editor in wxGUI Graphical Modeler.</i>
</center>

<p>
In the example below the <a href="http://e4ftl01.cr.usgs.gov/MOLT/MOD13Q1.006/">MODIS MOD13Q1</a>
(NDVI) satellite data products are used in a loop. The original data are 
stored as coded integer values that need to be multiplied by the
value <tt>0.0001</tt> to represent real <i>ndvi values</i>. Moreover, GRASS GIS
provides a predefined color table called <tt>ndvi</tt> to represent <i>ndvi data</i>.  
In this case it is not necessary to work with every image separately. 
<br>
The Graphical Modeler is an appropriate tool to 
process data in an effective way using loop and variables (<tt>%map</tt> for a 
particular MODIS image in mapset and <tt>%ndvi</tt> for original data name suffix). 
After the loop component is added to model, it is necessary to define series of maps
with required settings of map type, mapset, etc.

<center>
<a href="g_gui_gmodeler_loop_dlg.png">
<img src="g_gui_gmodeler_loop_dlg.png" width="300"></a>
<br>
<i>Figure: MODIS data representation in GRASS GIS after Graphical Modeler usage.</i>
</center>

<p>
When the model is supplemented by all of modules, these modules should be 
ticked in the boxes of loop dialog. The final model and its results are shown below.

<center>
<a href="g_gui_gmodeler_loop_final.png">
<img src="g_gui_gmodeler_loop_final.png" width="400"></a>
<br>
<i>Figure: Model with loop.</i>
</center>

<p>
<center>
<a href="g_gui_gmodeler_modis_1o.png">
<img src="g_gui_gmodeler_modis_1o.png" width="300"></a>
<a href="g_gui_gmodeler_modis_1.png">
<img src="g_gui_gmodeler_modis_1.png" width="300"></a>
<a href="g_gui_gmodeler_modis_2o.png">
<img src="g_gui_gmodeler_modis_2o.png" width="300"></a><br>
<a href="g_gui_gmodeler_modis_2.png">
<img src="g_gui_gmodeler_modis_2.png" width="300"></a>
<a href="g_gui_gmodeler_modis_3o.png">
<img src="g_gui_gmodeler_modis_3o.png" width="300"></a>
<a href="g_gui_gmodeler_modis_3.png">
<img src="g_gui_gmodeler_modis_3.png" width="300"></a>
<br>
<i>Figure: MODIS data representation in GRASS GIS after Graphical Modeler usage.</i>
</center>

<p>
The steps to enter in the command console would be as follows:

<div class="code"><pre>
# rename original image with preselected suffix
g.rename raster = %map,%map.%ndvi
# convert integer values
r.mapcalc expression = %map = %map.%ndvi * 0.0001
# set color table appropriate for nvdi data
r.colors = map = %map color = ndvi
</pre></div>

<h2>SEE ALSO</h2>

<em>
  <a href="wxGUI.html">wxGUI</a><br>
  <a href="wxGUI.components.html">wxGUI components</a>
</em>

<p>
See also selected user models available from this 
<a href="http://svn.osgeo.org/grass/grass-addons/grass7/models">SVN repository</a>.

<p>
See also
the <a href="http://grasswiki.osgeo.org/wiki/WxGUI_Graphical_Modeler">wiki</a> page
(especially various <a href="http://grasswiki.osgeo.org/wiki/WxGUI_Graphical_Modeler#Video_tutorials">video
tutorials</a>).

<h2>AUTHORS</h2>

Martin Landa, OSGeoREL, Czech Technical University in Prague, Czech Republic<br>
Various manual improvements by Ludmila Furkevicova, Slovak University of Technology in Bratislava, Slovak Republic

<p>
<i>$Date$</i>
